
# **HIBP**

Backend to mock Have I Been Pwned application

# **Prepare**

1) Replace placeholder values in `sample-keys.json` with real key values and rename file to `keys.json`

2) `yarn`


# **Use**

Start server with

`yarn dev`

Server is served on port 3333


# **Requires**

Node v. 15.x

# Technologies

React, Redux, Redux Toolkit
