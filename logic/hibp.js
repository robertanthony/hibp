
// have i been pwned mock data: just testing to see if the email contains 'pwn'
// if so, it's been pwned.
// obviously a better method would be to check against a sample db/file
// but really, the best solution would be to call the API
// which is prevented, due to API key, so this works for now.
//
// also: there is no check that this is a valid email at the moment...

export default (email) => {
  if (!email || typeof email !== 'string') return false;
  const tester = new RegExp(/pwn/)
  return tester.test(email)
}
