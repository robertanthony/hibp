import admin from 'firebase-admin';
import {promises as fs} from "fs";


let db = null;

const loadJson = async (filename) => {
  const data = await fs.readFile(filename, 'utf8');
  return JSON.parse(data);

}


export async function firebaseInit() {

  const serviceAccount = await loadJson('./keys.json')

  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });

  db = admin.firestore();

}


export const getSnapshot = async () => {
  if (!db) return null;
  const snapshot = await db.collection('users').get()
  const data = snapshot.docs.map(doc => doc.data())

  return data;
}

export const storeData = async (name, surname, email, isBreached) => {
  if (!db) return null;
  try {
    if (isBreached) {
      return await db.collection('users').add({
        name,
        surname,
        email,
        isBreached
      });
    } else {
      return await db.collection('users').add({
        name,
        surname,
        email
      });
    }

  } catch {
    console.log("Error writing data to Firestore");
    return null;
  }


}
