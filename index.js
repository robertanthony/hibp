import express from 'express'

import hibp from './logic/hibp.js'
import cors from 'cors'
import {firebaseInit, getSnapshot, storeData} from './firebase/index.js'

const PORT = 3333;

await firebaseInit();

const app = express()
app.use(express.json());

app.use(cors());


app.post("/email", async (req, res) => {

  const body = req.body;
  if (!body || !body.email || !body.name || !body.surname) return res.status(500).send('invalid data')
  const {email, name, surname} = body;
  const isBreached = hibp(email)
  const result = await storeData(name, surname, email, isBreached)
  if (result) {
    return res.status(200).send("OK")

  } else {
    return res.status(500).send('Server Error')

  }
});


app.get("/data", async (req, res) => {
  const data = await getSnapshot();
  return res.status(200).send(data);
});

app.listen(PORT, () => {
  console.log(`🚀 HIBP app listening at http://localhost:${PORT}`)
})

